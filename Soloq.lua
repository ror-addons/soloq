Soloq = Soloq or {};
Soloq.db = Soloq.db or {};

-- locals for performance
local tostring = tostring;
local towstring = towstring;
local tonumber = tonumber;
local StringSplit = StringSplit;
local string_find = string.find;
local find = find;
local TextLogGetEntry = TextLogGetEntry;
local TextLogGetNumEntries = TextLogGetNumEntries;
local TextLogAddEntry = TextLogAddEntry;
local TextLogSaveLog = TextLogSaveLog;

local playerName;
local rankedScenarioID;

local CHAT_PREFIX = "[Soloq]";
local PREFIX_COLOR = {r=50, g=255, b=10};

function Soloq.OnInitialize()

	-- run .sorenable before .mmrenable or else updates don't work 
	if not (ZonepopLite or RoR_SoR) then
		Soloq.enableSoR();
	end

	Soloq.enableMmr();

	playerName = Soloq.FixName( GameData.Player.name );
	Soloq.playerName = playerName;

	Soloq.loadSettings(playerName);

	Soloq.initializeOverview();
	Soloq.createOverwiewWindow();
	
	LayoutEditor.RegisterWindow ("SoloqOverviewWindow", L"Soloq", L"Soloq", false, false, true, nil);

	CHAT_PREFIX = Soloq.ToRgbWstring( PREFIX_COLOR, "[Soloq]" );
	previousZone = GameData.Player.zone;

	RegisterEventHandler( SystemData.Events.SCENARIO_POST_MODE, "Soloq.ProcessScenarioEnd" );
	RegisterEventHandler( TextLogGetUpdateEventId("Chat"), "Soloq.OnChatLogUpdated" );

	RegisterEventHandler( SystemData.Events.LOADING_END, "Soloq.onLoadingEnd");

	if LibSlash then
		LibSlash.RegisterSlashCmd("soloq", function(args) Soloq.SlashCmd(args) end);
		Soloq.Print( CHAT_PREFIX .. L" Addon initialized. Use /soloq to toggle." );
	else
		Soloq.Print( CHAT_PREFIX .. L" Addon initialized. Use '/script Soloq.toggleOverviewWindow()' macro to toggle." );
	end
	
end

function Soloq.enableMmr()
	SendChatText(L".mmrenable", ChatSettings.Channels[0].serverCmd);
end

function Soloq.requestQueueStatusUpdate()
	SendChatText(L".uiscstatus", ChatSettings.Channels[0].serverCmd);
end

function Soloq.enableSoR()
	SendChatText(L".sorenable", ChatSettings.Channels[0].serverCmd);
end

local TIME_DELAY = 2;
local timeLeft = 2;
function Soloq.OnUpdate(elapsed)

	timeLeft = timeLeft - elapsed;
    if (timeLeft > 0) then return end
	
	Soloq.requestQueueStatusUpdate();

	Soloq.updateMmr();
	Soloq.updateMmrLabels();

	timeLeft = TIME_DELAY;

end


function Soloq.ProcessScenarioEnd()
	if ( (GameData.ScenarioData.mode == GameData.ScenarioMode.POST_MODE) and not GameData.Player.isInSiege ) then
        Soloq.CountScenarioEnding();
	end
end

function Soloq.CountScenarioEnding()

	-- check if we played ranked solo
	local scenarioName = GetScenarioName(GameData.ScenarioData.id);
	if ( not scenarioName:find(L"Ranked Solo") ) then return end

	-- Check which faction won
	local victorRealm;
	if (GameData.ScenarioData.orderPoints > GameData.ScenarioData.destructionPoints) then
		victorRealm = GameData.Realm.ORDER;
	elseif (GameData.ScenarioData.orderPoints < GameData.ScenarioData.destructionPoints) then
		victorRealm = GameData.Realm.DESTRUCTION;
	else
		victorRealm = GameData.Realm.NONE;
	end

	Soloq.db[playerName].soloGamesPlayed = Soloq.db[playerName].soloGamesPlayed + 1;

	-- There is a winner
    if (victorRealm ~= GameData.Realm.NONE) then
		if (victorRealm == GameData.Player.realm) then
			Soloq.db[playerName].soloGamesWon = Soloq.db[playerName].soloGamesWon + 1;
		else
			Soloq.db[playerName].soloGamesLost = Soloq.db[playerName].soloGamesLost + 1;
		end
    else
        -- It's a tie
		Soloq.db[playerName].soloGamesDrawn = Soloq.db[playerName].soloGamesDrawn + 1;
    end	

	Soloq.updateMatchHistoryLabels();
	Soloq.updateWinrateLabel();

end


local soloqUpdateCache;
function Soloq.OnChatLogUpdated(updateType, filterType)
	
	if (updateType ~= SystemData.TextLogUpdate.ADDED) then return end
	if (filterType ~= SystemData.ChatLogFilters.CHANNEL_9) then return end

	local _, _, text = TextLogGetEntry( "Chat", TextLogGetNumEntries("Chat") - 1 );
	text = tostring(text);

	if (text == soloqUpdateCache) then return end

	if string_find(text, "SCPlayers:") then
		Soloq.processQueueUpdate(text);
		soloqUpdateCache = text;
	end
	
end


local showOverviewWindowOnNextZone;
local caledorWoodsZoneID = 237;
-- hide overview window when entering ranked sc and then restore previous visibility after leaving it
function Soloq.onLoadingEnd()
	
	if (not rankedScenarioID) then return end

	local newZone = GameData.Player.zone;
	if(newZone == 0) or (GameData.ScenarioData.id == 0) then return end

	if (GameData.ScenarioData.id == rankedScenarioID) and (newZone == caledorWoodsZoneID) then
		Soloq.onEnterRankedScenario();
	else
		Soloq.onEnterRegularZone();
	end

end

function Soloq.onEnterRankedScenario()
	-- d('Soloq.onEnterRankedScenario()');
	showOverviewWindowOnNextZone = WindowGetShowing("SoloqOverviewWindow");
	Soloq.hideOverviewWindow();
end

function Soloq.onEnterRegularZone()
	-- d('Soloq.onEnterRegularZone()');
	if showOverviewWindowOnNextZone and (showOverviewWindowOnNextZone == true) then
		Soloq.showOverviewWindow();
		showOverviewWindowOnNextZone = nil;
	end
end


function Soloq.processQueueUpdate(text)

	-- SCPlayers:ScenarioId:OrderTankCount:OrderDPSCount:OrderHealerCount:DestroTankCount:DestroDPSCount:DestroHealerCount:activeGames
	local data = StringSplit(tostring(text), ":");
	rankedScenarioID = tonumber(data[2]);

	local orderTanks = data[3];
	local orderDPS = data[4];
	local orderHealers = data[5];

	local destroTanks = data[6];
	local destroDPS = data[7];
	local destroHealers = data[8];

	Soloq.updateQueueLabels(orderTanks, orderDPS, orderHealers, destroTanks, destroDPS, destroHealers);

	local activeGames = tonumber(data[9]);
	local orderQueued = tonumber(orderTanks) + tonumber(orderDPS) + tonumber(orderHealers);
	local destroQueued = tonumber(destroTanks) + tonumber(destroDPS) + tonumber(destroHealers);

	Soloq.updateHeaderLabels(activeGames, orderQueued, destroQueued);

end



function Soloq.updateMmr()

	local newSoloMmr = tonumber(LabelGetText("TomeWindowTitlePageStatSoloMMRNumber"));
	local newPremadeMmr = tonumber(LabelGetText("TomeWindowTitlePageStatPremadeMMRNumber"));

	-- tome of knowledge is being difficult
	if (newSoloMmr == 0) then return end

	-- only cache mmr if there has been an update
	if (Soloq.db[playerName].soloMmr ~= newSoloMmr) then
		Soloq.db[playerName].soloMmrCache = Soloq.db[playerName].soloMmr;
		Soloq.db[playerName].soloMmr = newSoloMmr;
	end	
	if (Soloq.db[playerName].premadeMmr ~= newPremadeMmr) then
		Soloq.db[playerName].premadeMmrCache = Soloq.db[playerName].premadeMmr;
		Soloq.db[playerName].premadeMmr = newPremadeMmr;
	end

end

function Soloq.loadSettings(playerName)

	Soloq.db[playerName] = Soloq.db[playerName] or {};

	Soloq.db[playerName].soloMmr = Soloq.db[playerName].soloMmr or 0;
	Soloq.db[playerName].premadeMmr = Soloq.db[playerName].premadeMmr or 0;
	Soloq.db[playerName].soloMmrCache = Soloq.db[playerName].soloMmrCache or 0;
	Soloq.db[playerName].premadeMmrCache = Soloq.db[playerName].premadeMmrCache or 0;

	Soloq.db[playerName].soloGamesPlayed = Soloq.db[playerName].soloGamesPlayed or 0;
	Soloq.db[playerName].soloGamesWon = Soloq.db[playerName].soloGamesWon or 0;
	Soloq.db[playerName].soloGamesLost = Soloq.db[playerName].soloGamesLost or 0;
	Soloq.db[playerName].soloGamesDrawn = Soloq.db[playerName].soloGamesDrawn or 0;

end



function Soloq.SlashCmd(args)

	local command;
	local parameter;
	local separator = string.find(args," ");
	
	if separator then
		command = string.sub(args, 0, separator - 1);
		parameter = string.sub(args, separator + 1, -1);
	else
		command = args;
	end
	
	if command == "" then Soloq.toggleOverviewWindow();
	else Soloq.Print( CHAT_PREFIX .. L" Unknown command." );
	end
	
end







