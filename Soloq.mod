<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="Soloq" version="1.0" date="18/01/2020" >
		<Author name="Caffeine" />
		<Description text="An addon to track Ranked Solo queue status and to record played games. Use /soloq to toggle." />
		<VersionSettings gameVersion="1.4.8" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Dependencies>
			<Dependency name="EA_TomeOfKnowledge" />
			<Dependency name="EA_ChatSystem" />
			<Dependency name="EA_ChatWindow" />
			<Dependency name="LibSlash" />
			<!-- <Dependency name="ZonepopLite" /> -->
		</Dependencies>
		<Files>
			<File name="Utils.lua" />
			<File name="Soloq.lua" />
			<File name="ui/Overview.lua" />
            <File name="ui/Overview.xml" />
		</Files>
		<OnInitialize>
			<CallFunction name="Soloq.OnInitialize" />
		</OnInitialize>
		<OnUpdate>
			<CallFunction name="Soloq.OnUpdate" />
		</OnUpdate>
		<SavedVariables>
			<SavedVariable name="Soloq.db" global="false"/>
		</SavedVariables>
        <OnShutdown>
        </OnShutdown>
		<WARInfo>    
		  <Categories>
			<Category name="Scenarios" />
		  </Categories>
		</WARInfo>		
	</UiMod>
</ModuleFile>