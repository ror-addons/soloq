Soloq = Soloq or {};

function Soloq.ToRgbWstring(color, str)
	return towstring(CreateHyperLink(L"", towstring(str), {color.r, color.g, color.b}, {}));
end

function Soloq.Print(str)
	EA_ChatWindow.Print(towstring(str));
end

function Soloq.FixName(str)
	if not str then return nil end
	
	local str = str;
	if (type(str) ~= "wstring") then str = towstring(str) end
	
	local pos = str:find (L"^", 1, true);
	if pos then str = str:sub (1, pos - 1) end
	
	pos = str:find (L" ", 1, true)
	if pos then str = str:sub (1, pos - 1) end
	
	return str;
end