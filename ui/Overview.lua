Soloq = Soloq or {};

-- lua locals for performance
local towstring = towstring;
local tonumber = tonumber
local LabelSetText = LabelSetText;
local LabelGetText = LabelGetText;
local math_floor = math.floor

local COLOR_ORDER = {r=0, g=205, b=255};
local COLOR_DESTR = {r=207, g=42, b=39};

local COLOR_NET_GAIN = {r=57, g=255, b=20};
local COLOR_NET_LOSS = {r=255, g=7, b=58};

local playerName;
local overviewWindowName = "SoloqOverviewWindow";

function Soloq.initializeOverview()
	playerName = Soloq.playerName;
end

-- /script Soloq.createOverwiewWindow()
function Soloq.createOverwiewWindow()

	if (DoesWindowExist(overviewWindowName) == true) then return end

	CreateWindow(overviewWindowName, false);

	Soloq.setStaticLabels();
	Soloq.updateMatchHistoryLabels();
	Soloq.updateWinrateLabel();
	Soloq.updateMmrLabels();

end

-- /script Soloq.toggleOverviewWindow()
function Soloq.toggleOverviewWindow()
	local isVisible = WindowGetShowing(overviewWindowName);
	WindowSetShowing(overviewWindowName, not isVisible);
end

function Soloq.hideOverviewWindow()
	WindowSetShowing(overviewWindowName, false);
end


function Soloq.showOverviewWindow()
	WindowSetShowing(overviewWindowName, true);
end


-- /script d(Soloq.updateMatchHistoryLabels())
function Soloq.updateMatchHistoryLabels()
	LabelSetText( overviewWindowName .. "StatsPlayerRecordedValue", towstring(Soloq.db[playerName].soloGamesPlayed));
	LabelSetText( overviewWindowName .. "StatsPlayerGamesWonValue", towstring(Soloq.db[playerName].soloGamesWon));
	LabelSetText( overviewWindowName .. "StatsPlayerGamesLostValue", towstring(Soloq.db[playerName].soloGamesLost));
	LabelSetText( overviewWindowName .. "StatsPlayerGamesDrawnValue", towstring(Soloq.db[playerName].soloGamesDrawn));

end


function Soloq.updateMmrLabels()

	LabelSetText( overviewWindowName .. "StatsPlayerMMRSoloValue", towstring(Soloq.db[playerName].soloMmr) );
	LabelSetText( overviewWindowName .. "StatsPlayerMMRGroupValue", towstring(Soloq.db[playerName].premadeMmr) );

	local function buildNetString(oldValue, newValue)
		local net = newValue - oldValue;
		if ( (oldValue == 0) or (net == 0) ) then return L"~" end
		if (net > 0) then
			return Soloq.ToRgbWstring( COLOR_NET_GAIN, L"+"..towstring(net) );
		else
			return Soloq.ToRgbWstring( COLOR_NET_LOSS, towstring(net) );
		end
	end
	
	LabelSetText( overviewWindowName .. "StatsPlayerMMRSoloNet", buildNetString(Soloq.db[playerName].soloMmrCache, Soloq.db[playerName].soloMmr));
	LabelSetText( overviewWindowName .. "StatsPlayerMMRGroupNet", buildNetString(Soloq.db[playerName].premadeMmrCache, Soloq.db[playerName].premadeMmr));

end

function Soloq.setStaticLabels()

	-- player stats labels
	LabelSetText( overviewWindowName .. "StatsPlayerName", Soloq.FixName( GameData.Player.name ));
	LabelSetText( overviewWindowName .. "StatsPlayerMMR", L"MMR");
	LabelSetText( overviewWindowName .. "StatsPlayerMMRSoloText", L"Solo");
	LabelSetText( overviewWindowName .. "StatsPlayerMMRGroupText", L"Group");
	LabelSetText( overviewWindowName .. "StatsPlayerRecordedText", L"Recorded");
	LabelSetText( overviewWindowName .. "StatsPlayerGamesWonText", L"Wins");
	LabelSetText( overviewWindowName .. "StatsPlayerGamesLostText", L"Losses");
	LabelSetText( overviewWindowName .. "StatsPlayerGamesDrawnText", L"Draws");

	-- queue status labels
	LabelSetText( overviewWindowName .. "QueueStatusText", L"Queue Status");
	LabelSetText( overviewWindowName .. "QueueStatusTankText", L"TANK");
	LabelSetText( overviewWindowName .. "QueueStatusDpsText", L"DPS");
	LabelSetText( overviewWindowName .. "QueueStatusHealText", L"HEAL");

end

-- /script Soloq.updateWinrateLabel()
function Soloq.updateWinrateLabel()

	local winrate;
	local gamesPlayed = Soloq.db[playerName].soloGamesLost+Soloq.db[playerName].soloGamesWon;

	if (gamesPlayed == 0) then
		winrate = 0;
	else
		winrate = math_floor( 100 * (Soloq.db[playerName].soloGamesWon / gamesPlayed ) );
	end

	LabelSetText( overviewWindowName .. "StatsPlayerWinrate", towstring(winrate) .. L"% win rate");

end

function Soloq.updateHeaderLabels(activeGames, orderQueued, destroQueued)

	if (activeGames == 1) then
		LabelSetText( overviewWindowName .. "HeaderActiveGamesLabel", L"1 game in progress");
	else
		LabelSetText( overviewWindowName .. "HeaderActiveGamesLabel", towstring(activeGames) .. L" games in progress");
	end

	local total = destroQueued + orderQueued;
	total = towstring(total);

	local order = Soloq.ToRgbWstring( COLOR_ORDER, orderQueued );
	local des = Soloq.ToRgbWstring( COLOR_DESTR, destroQueued );

	LabelSetText( overviewWindowName .. "HeaderQueuedPlayersLabel", total .. L" [ " .. order .. L" / " .. des .. L" ] players in queue");

end


function Soloq.updateQueueLabels(orderTanks, orderDPS, orderHealers, destroTanks, destroDPS, destroHealers)

	LabelSetText( overviewWindowName .. "QueueStatusOrderTankValue", towstring(orderTanks));
	LabelSetText( overviewWindowName .. "QueueStatusOrderDpsValue", towstring(orderDPS));
	LabelSetText( overviewWindowName .. "QueueStatusOrderHealValue", towstring(orderHealers));

	LabelSetText( overviewWindowName .. "QueueStatusDestroTankValue", towstring(destroTanks));
	LabelSetText( overviewWindowName .. "QueueStatusDestroDpsValue", towstring(destroDPS));
	LabelSetText( overviewWindowName .. "QueueStatusDestroHealValue", towstring(destroHealers));

end


















